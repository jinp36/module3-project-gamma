import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

import axios from "axios";

export default function UserForm() {
  const navigate = useNavigate();

  const [formData, setFormData] = useState({
    first: "",
    last: "",
    username: "",
    email: "",
    password: "",
    zodiac_sign: "",
  });

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    axios.post(`${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/users`, formData)
      .then(response => {

        setFormData({
          first: "",
          last: "",
          username: "",
          email: "",
          password: "",
          zodiac_sign: ""

        });


        navigate(`/login`)
      })
      .catch(err => { console.log(err) })
    e.target.reset();
  };




  return (
    <div className="row">
      <div className="offset-3 col-6 mb-5 mt-5" style={{ backgroundColor: "rgba(255, 255, 255, 0.7)", borderRadius: "10px" }}>
        <div className="shadow p-4 mt-3">
          <h1 className="text-center">Create an Account</h1>
          <form onSubmit={handleSubmit} id="create-user">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.first}
                placeholder="first"
                required
                type="text"
                name="first"
                id="first"
                className="form-control"
              />
              <label htmlFor="first">First Name</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.last}
                placeholder="last"
                required
                type="text"
                name="last"
                id="last"
                className="form-control"
              />
              <label htmlFor="last">Last Name</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.username}
                placeholder="username"
                required
                type="text"
                name="username"
                id="username"
                className="form-control"
              />
              <label htmlFor="username">Username</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.email}
                placeholder="email"
                required
                type="email"
                name="email"
                id="email"
                className="form-control"
              />
              <label htmlFor="email">Email</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.password}
                placeholder="password"
                required
                type="password"
                name="password"
                id="password"
                className="form-control"
              />
              <label htmlFor="password">Password</label>
            </div>

            <div className="form-floating mb-3">
              <select
                onChange={handleFormChange}
                value={formData.zodiac_sign}
                name="zodiac_sign"
                id="zodiac_sign"
                className="form-select">
                <option value="" >Zodiac Sign</option>
                <option value="Aries">Aries</option>
                <option value="Taurus">Taurus</option>
                <option value="Gemini">Gemini</option>
                <option value="Cancer">Cancer</option>
                <option value="Leo">Leo</option>
                <option value="Virgo">Virgo</option>
                <option value="Libra">Libra</option>
                <option value="Scorpio">Scorpio</option>
                <option value="Sagittarius">Sagittarius</option>
                <option value="Capricorn">Capricorn</option>
                <option value="Aquarius">Aquarius</option>
                <option value="Pisces">Pisces</option>
              </select>
            </div>

            <button type="submit" className="btn btn-primary">
              Create an Account
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}
