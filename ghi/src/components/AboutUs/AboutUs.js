import "./AboutUs.css";
import brad from "./Brad.jpg";
import fermin from "./Fermin.JPG";
import jin from "./Jin.jpg";
import myla from "./Myla.jpg";


function AboutUs() {
  return (
    <div className="about-us-responsive-container-block bigContainer">
      <div className="about-us-responsive-container-block Container" style={{ backgroundColor: "rgba(255, 255, 255, 0.7)", borderRadius: "10px" }}>
        <p className="about-us-text-blk heading">About Us</p>
        <p className="about-us-text-blk subHeading"><i>At JournE, our mission is to connect people with the vibrant tapestry of their surroundings, providing a platform for sharing and discovering local hotspots. We empower individuals to showcase their cities, fostering a sense of exploration and community through captivating visuals and interactive maps. With JournE, we strive to inspire and guide tourists and locals alike, unlocking the hidden gems that make every city unique.</i></p>
        <section className="about-us-responsive-container-block Container">
          <h2 className="about-us-text-blk heading">Meet Our Founders & Developers</h2>
          <div className="about-us-container">
            <div className="about-us-profile">
              <a href="https://www.linkedin.com/in/mylasmith/"><img src={myla} alt="myla" />
                <button className="about-us-name">Myla Smith</button></a>
            </div>
            <div className="about-us-profile">
              <a href="https://www.linkedin.com/in/bradleyallenprofile/">
                <img
                  src={brad}
                  alt="brad"
                />
                <button className="about-us-name">Brad Allen</button>
              </a>
            </div>

            <div className="about-us-profile">
              <a href="https://www.linkedin.com/in/jinpark7/">
                <img
                  src={jin}
                  alt="jin"
                />
                <button className="about-us-name">Jin Park</button>
              </a>
            </div>
            <div className="about-us-profile">
              <a href="https://www.linkedin.com/in/ferminsantiago/">
                <img
                  src={fermin}
                  alt="fermin"
                />
                <button className="about-us-name">Fermin Santiago</button>
              </a>
            </div>
          </div>
        </section>
        <div className="about-us-social-icons-container">
          <div className="about-us-social-icon">
            <img
              className="about-us-socialIcon image-block"
              src="https://i.pinimg.com/564x/a8/45/df/a845df2e57a47b2783ed98a811400c68.jpg"
              alt="instagram"
            />
          </div>
          <div className="about-us-social-icon">
            <img
              className="about-us-socialIcon image-block"
              src="https://i.pinimg.com/736x/c2/37/fd/c237fda3b372a862344479ae8be1ed1e.jpg"
              alt="facebook"
            />
          </div>
          <div className="about-us-social-icon">
            <img
              className="about-us-socialIcon image-block"
              src="https://i.pinimg.com/564x/92/ee/93/92ee93fb95a38c6a090093a770158746.jpg"
              alt="twitter"
            />
          </div>
          <div className="about-us-social-icon">
            <img
              className="about-us-socialIcon image-block"
              src="https://i.pinimg.com/564x/ed/ff/07/edff071f841239e2013f44f926c1bc3e.jpg"
              alt="pinterest"
            />
            <div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AboutUs;
