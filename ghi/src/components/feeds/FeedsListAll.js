import React, { useState, useEffect } from "react";
import './posts.css';

export default function FeedsListAll() {
    const [posts, setPosts] = useState([]);


    const getPosts = async () => {
        const response = await fetch(`${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/feeds`);
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setPosts(data.feeds);
        }
    };

    useEffect(() => {
        getPosts();
    }, []);

    return (
        <>
            <section className="hero-section">
                <div className="card-grid">
                    {posts.map((post) => (
                        <div className="card" key={post.id}>
                            <div
                                className="card__background"
                                style={{ backgroundImage: `url(${post.picture_url})` }}
                            ></div>
                            <div className="card__content">
                                <p className="text-with-shadow">{post.category}</p>
                                <h3 className="text-with-shadow">{post.subject}</h3>
                                <h5 className="text-with-shadow">{post.description}</h5>

                            </div>
                        </div>
                    ))}
                </div>
            </section>
        </>
    );

}
