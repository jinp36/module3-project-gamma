from pydantic import BaseModel
from fastapi import APIRouter, Depends, Response, Query
from typing import Optional, List
from queries.feeds import FeedQueries
from datetime import date

router = APIRouter()


class FeedIn(BaseModel):
    user_id: int
    subject: Optional[str]
    favorite: Optional[bool]
    longitude: float
    latitude: float
    post_date: date
    category: str
    picture_url: str
    description: str
    id: Optional[int]


class FeedOut(BaseModel):
    id: int
    user_id: int
    subject: Optional[str]
    favorite: Optional[bool]
    longitude: float
    latitude: float
    post_date: date
    category: str
    picture_url: str
    description: str


class FeedsOut(BaseModel):
    feeds: list[FeedOut]


@router.get("/api/feeds", response_model=FeedsOut)
def feeds_list(queries: FeedQueries = Depends()):
    return {
        "feeds": queries.get_all_feeds(),
    }


@router.get("/api/locations", response_model=FeedsOut)
def locations(
    from_lat: float = Query(...),
    to_lat: float = Query(...),
    from_lng: float = Query(...),
    to_lng: float = Query(...),
    queries: FeedQueries = Depends(),
):
    results = queries.get_feeds_by_coordinates(
        from_lat, to_lat, from_lng, to_lng
    )
    feeds_out = FeedsOut(feeds=results)
    return feeds_out


@router.get("/api/feeds/{feed_id}", response_model=FeedOut)
def get_feed(
    feed_id: int,
    response: Response,
    queries: FeedQueries = Depends(),
):
    record = queries.get_feed(feed_id)
    if record is None:
        response.status_code = 404
    else:
        return record


@router.get("/api/specificfeeds/{user_id}", response_model=List[FeedOut])
def get_feeds_by_user_id(
    user_id: int,
    response: Response,
    queries: FeedQueries = Depends(),
):
    record = queries.get_feeds_by_userid(user_id)
    if record is not None:
        return record
    return []


@router.post("/api/feeds", response_model=FeedOut)
def create_feed(feed_in: FeedIn, queries: FeedQueries = Depends()):
    return queries.create_feed(feed_in)


@router.put("/api/feeds/{feed_id}")
def update_feed(
    feed_id: int,
    feed_in: FeedIn,
    response: Response,
    queries: FeedQueries = Depends(),
):
    record = queries.update_feed(feed_id, feed_in)
    if record is None:
        response.status_code = 404
    else:
        return record


@router.delete("/api/feeds/{feed_id}", response_model=bool)
def delete_user(feed_id: int, queries: FeedQueries = Depends()):
    queries.delete_feed(feed_id)
    return True
