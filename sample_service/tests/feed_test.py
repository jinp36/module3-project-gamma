from fastapi.testclient import TestClient
from queries.feeds import FeedQueries
import psycopg2

from main import app

client = TestClient(app)


class FeedQueriesMock:
    def get_all_feeds(self):
        return [
            {
                "id": 2,
                "user_id": 2,
                "subject": "test",
                "longitude": 55,
                "latitude": 55,
                "favorite": True,
                "post_date": "2022-01-01",
                "category": "Event",
                "picture_url": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQsN5IsRm0vnePwC2Ig6SgQIxrY-uAZi6FQcw&usqp=CAU",
                "description": "lorem ipsum",
            },
            {
                "id": 1,
                "user_id": 1,
                "subject": "test",
                "longitude": 55,
                "latitude": 55,
                "favorite": True,
                "post_date": "2022-01-01",
                "category": "Event",
                "picture_url": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQsN5IsRm0vnePwC2Ig6SgQIxrY-uAZi6FQcw&usqp=CAU",
                "description": "lorem ipsum",
            },
        ]


def test_get_all_feeds():
    app.dependency_overrides[FeedQueries] = FeedQueriesMock

    response = client.get("api/feeds")

    assert response.status_code == 200
    assert len(response.json()) >= 1

    app.dependency_overrides = {}
